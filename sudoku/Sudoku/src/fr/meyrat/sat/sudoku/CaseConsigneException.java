package fr.meyrat.sat.sudoku;

public class CaseConsigneException extends Exception {

	/**
	 * Exception qui sera levée lorsque l'on ne prend pas en compte le fait qu'une case soit consigne
	 */
	private static final long serialVersionUID = 1511146128159784214L;

}
