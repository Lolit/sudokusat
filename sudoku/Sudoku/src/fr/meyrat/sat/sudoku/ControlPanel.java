package fr.meyrat.sat.sudoku;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ControlPanel extends JPanel{
    
    /**
     * Panel contenat les boutonsde contrôle de l'application
     */
    private static final long serialVersionUID = -5278161239510620045L;
    
    Main parent;

    public ControlPanel(Main parent){
        this.parent = parent;
        
        JButton resoudre = new JButton("résoudre");
        resoudre.addActionListener(e->{
            parent.resoudre();
          
        });
        JButton reset = new JButton("réinitialiser");
        reset.addActionListener(e->{
            parent.reinitialiser();
          
        });
        
        add(resoudre);
        add(reset);
    }
    /*
     * Ecrit Xi==A à la n-ième place dans la Clause Sudoku
     */
    public static void writeXijequalsA(int[] data,int A, int n ){
        
    }

}
