package fr.meyrat.sat.sudoku;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import fr.meyrat.sat.utils.AndFormula;
import fr.meyrat.sat.utils.Formula;
import fr.meyrat.sat.utils.OrFormula;
import fr.meyrat.sat.utils.VarFormula;

public class GrilleSudoku extends JPanel {

	private static final long serialVersionUID = 5994781992738054701L;
	private JFormattedTextField[][] grille;
	private JPanel[] grillePans;

	public GrilleSudoku() {
		JPanel grillePanel = new JPanel();

		GridLayout lay = new GridLayout(3, 3, 4, 4);
		grillePanel.setLayout(lay);
		grillePanel.setBackground(Color.BLACK);
		grillePanel.setPreferredSize(new Dimension(300, 320));

		grillePans = new JPanel[9];

		for (int i = 0; i < 9; i++) {
			grillePans[i] = new JPanel();
			grillePans[i].setLayout(new GridLayout(3, 3, 2, 2));
			grillePanel.add(grillePans[i]);
		}

		add(grillePanel);

		grille = new JFormattedTextField[9][9];
		try {
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {

					grille[i][j] = new JFormattedTextField(new MaskFormatter("#"));
					grille[i][j].setHorizontalAlignment(JTextField.CENTER);
					grille[i][j].setFont(new Font(" TimesRoman ", Font.BOLD, 20));
					grille[i][j].setForeground(Color.RED);
					grille[i][j].setCaretPosition(0);
					grillePans[i / 3 * 3 + (j / 3)].add(grille[i][j]);
					grille[i][j].setColumns(1);
				}
			}
		} catch (ParseException e) {
			// mask formatter
			e.printStackTrace();
		}

	}

	public byte getij(int i, int j) {
		return Byte.parseByte(grille[i][j].getText());

	}

	public void reinitialiser() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				grille[i][j].setText(null);
				grille[i][j].setForeground(Color.RED);
			}
		}

	}

	public Formula toFormula() {
		// Xn : n%9 est la valeur de la case (n/3%9, n/3/9)
		ArrayList<Formula> clauses = new ArrayList<>();
		// Toutes les cases doivent être remplies
		int[] tmpVars = new int[9];
		int tmp = 0;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				tmp = caseijEgalkp1(i, j, 0);
				for (int k = 0; k < 9; k++) {
					tmpVars[k] = tmp + k;
				}
				clauses.add(new OrFormula(tmpVars));
			}
		}

		// Toutes les cases doivent être remplies par une unique valeur
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 8; k++) {
					for (int l = k + 1; l < 9; l++) {
						// non ET : pas deux valeurs dans une case
						clauses.add(new OrFormula(new int[] { -caseijEgalkp1(i, j, k), -caseijEgalkp1(i, j, l) }));
					}
				}
			}
		}

		// une seule valeur par ligne
		for (int i = 0; i < 9; i++) {
			for (int k = 0; k < 9; k++) {
				for (int j = 0; j < 8; j++) {
					for (int l = j + 1; l < 9; l++) {
						clauses.add(new OrFormula(new int[] { -caseijEgalkp1(i, j, k), -caseijEgalkp1(i, l, k) }));
					}
				}
			}
		}
		// une seule valeur par colonne
		for (int j = 0; j < 9; j++) {
			for (int k = 0; k < 9; k++) {
				for (int i = 0; i < 8; i++) {
					for (int l = i + 1; l < 9; l++) {
						clauses.add(new OrFormula(new int[] { -caseijEgalkp1(i, j, k), -caseijEgalkp1(l, j, k) }));
					}
				}
			}
		}

		// une seule valeur par case
		for (int c = 0; c < 9; c++) {
			for (int k = 0; k < 9; k++) {
				for (int j = 0; j < 8; j++) {
					for (int l = j + 1; l < 9; l++) {
						clauses.add(new OrFormula(new int[] { -caseijEgalkp1(c / 3 * 3 + j / 3, c % 3 * 3 + j % 3, k),
								-caseijEgalkp1(c / 3 * 3 + l / 3, c % 3 * 3 + l % 3, k) }));
					}
				}
			}
		}

		// consignes
		String tmpTxt = "";
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				tmpTxt = grille[i][j].getText();
				if (!tmpTxt.equals(" ")) {
					clauses.add(new VarFormula(caseijEgalkp1(i, j, Integer.parseInt(tmpTxt) - 1)));
				}
			}
		}

		return new AndFormula(clauses);

	}

	private int caseijEgalkp1(int i, int j, int k) {
		return 9 * (i * 9 + j) + k + 1;// on ne peut pas utiliser 0 comme nom de
										// variable
	}

	public void afficherSolution(int[] solution) {
		// solution est l'état de chaque variable booléenne
		int i = 0, j = 0, val = 0;
		for (int sol : solution) {
			if (sol > 0) {
				val = (sol - 1) % 9 + 1;
				i = (sol - 1) / 9 / 9;
				j = (sol - 1) / 9 % 9;
				if (grille[i][j].getText().equals(" ")) {
					grille[i][j].setForeground(Color.black);
					grille[i][j].setText("" + val);
				}
			}
		}
	}
}
