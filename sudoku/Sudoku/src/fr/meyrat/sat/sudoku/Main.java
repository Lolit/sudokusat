package fr.meyrat.sat.sudoku;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.TimeoutException;

import fr.meyrat.sat.utils.AndFormula;
import fr.meyrat.sat.utils.Formula;

public class Main extends JFrame{
    
    /**
     * 
     */
    private static final long serialVersionUID = -5495698941047833519L;
    GrilleSudoku grille;
    public Main(){
        super("Solveur de Sudoku");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(450, 360);
        setResizable(false);
        this.setLocationRelativeTo(null);
        
        //setLayout(new BorderLayout());
        
        grille = new GrilleSudoku();
        add(grille, BorderLayout.WEST);
        add(new ControlPanel(this));
        
        setVisible(true);
    }

    public static void main(String[] args) {
        new Main();

    }

    public void resoudre() {
        AndFormula prb = grille.toFormula().toCNFFormula();
        System.out.println(prb);
        try {
			IProblem solveur = Formula.cNFFormulaToSAT4J(prb, 9*9*9);
			if(solveur.isSatisfiable()){
				System.out.println("Problème résolu !");
				grille.afficherSolution(solveur.model());
			}else{
				System.out.println("Problème irrésoluble");
			}
		} catch (ContradictionException e) {
			System.out.println("Grille irrésoluble !");
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
        
        
    }

    public void reinitialiser() {
        grille.reinitialiser();
        
    }

}
