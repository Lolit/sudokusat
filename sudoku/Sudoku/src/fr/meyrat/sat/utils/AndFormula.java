package fr.meyrat.sat.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class AndFormula extends Formula {
	Formula[] p;

	public AndFormula(Formula... enfants) {
		p = enfants;
	}
	public AndFormula(int[] vals){
		p = new VarFormula[vals.length];
		for(int i=0; i<vals.length; i++){
			p[i] = new VarFormula(vals[i]);
		}
	}
	public void fusionner(AndFormula nouvelle){
		int l = p.length;
		p = Arrays.copyOf(p, l+nouvelle.p.length);
		System.arraycopy(nouvelle.p, 0, p, l, nouvelle.p.length);
	}
	public AndFormula(ArrayList<Formula> vals){
		p = new Formula[vals.size()];
		Iterator<Formula> it = vals.iterator();
		int i=0;
		while(it.hasNext()){
			p[i] = it.next();
			i++;
		}
	}


	@Override
	public String toString() {
		String res = "";
		for (Formula q : p) {
			if (!(q instanceof VarFormula)) {
				res += "(" + q.toString() + ") AND ";
			} else {
				res += q.toString() + " AND ";
			}
		}
		if(res.length()==0){
			System.out.println("Problème : And sans enfants");
			return "";
		}
		return res.substring(0, res.length() - 4);
	}

	@Override
	public void reduire() {
		ArrayList<Formula> pc = new ArrayList<>();
		for (int i = 0; i < p.length; i++) {
			p[i].reduire();
			if (p[i] instanceof AndFormula) {
				for (Formula f : ((AndFormula) p[i]).p) {
					pc.add(f);
				}
			}else{
				pc.add(p[i]);
			}
		}
		Formula[] pcp = new Formula[pc.size()];
		p = pc.toArray(pcp);

	}

	@Override
	public Formula copy() {
		Formula[] pc = new Formula[p.length];
		for (int i = 0; i < p.length; i++) {
			pc[i] = p[i].copy();
		}
		return new AndFormula(pc);
	}

}
