package fr.meyrat.sat.utils;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;

public abstract class Formula {
	// essai d'implémentation de la technique proposée sur
	// "http://cs.jhu.edu/~jason/tutorials/convert-to-CNF"

	/*
	 * Ne modifie pas la formule initiale
	 * Mécanisme interne de la méthode toCNFFormula : ne pas utiliser !
	 */
	protected AndFormula toCNFFormulaREC() {

		if (this instanceof AndFormula) {
			AndFormula fp = (AndFormula) this;
			Formula[] pc = new Formula[fp.p.length];
			for (int i =0; i< pc.length; i++) {
				
				if (!(fp.p[i] instanceof VarFormula)) {
					pc[i] = fp.p[i].toCNFFormulaREC();
				}else{
					pc[i] = new VarFormula(((VarFormula)fp.p[i]).var);
				}
			}
			return new AndFormula(pc);
		} else if (this instanceof OrFormula) {
			// fp est juste une redite de this avec le bon type
			OrFormula fp = (OrFormula) this;
			if (fp.p.length > 2) {
				Formula[] q = new Formula[fp.p.length - 1];
				for (int i = 1; i < fp.p.length; i++) {
					q[i - 1] = fp.p[i];
				}
				AndFormula ft = (new OrFormula(q)).toCNFFormulaREC();// on se ramène au cas q.length=2
				fp.p[0] = fp.p[0].toCNFFormulaREC();
				// on transforme (P1^P2^..) u (Q1^Q2^..) en sa forme
				// développée
				Formula[] tab1 = ((AndFormula) fp.p[0]).p;
				Formula[] tab2 = ft.p;
				int i = 0;
				Formula[] newQ = new Formula[tab1.length * tab2.length];
				for (Formula fpp : tab1) {
					for (Formula ftp : tab2) {
						newQ[i] = new OrFormula(fpp, ftp);
						i++;
					}
				}
				return new AndFormula(newQ);

			} else if (fp.p.length == 2) {
				Formula[] tab1 = fp.p[0].toCNFFormulaREC().p;
				Formula[] tab2 = fp.p[1].toCNFFormulaREC().p;
				int i = 0;
				Formula[] newQ = new Formula[tab1.length * tab2.length];
				for (Formula fpp : tab1) {
					for (Formula ftp : tab2) {
						newQ[i] = new OrFormula(fpp, ftp);
						i++;
					}
				}
				return new AndFormula(newQ);
			} else {
				return fp.p[0].toCNFFormulaREC();

			}

		} else if (this instanceof VarFormula) {
			return new AndFormula(new Formula[] { this });
		} else {
			System.err.println("Erreur de conversion en CNF");
			return null;
		}

	}

	// enlève les AND et OR redondants
	public abstract void reduire();
	public abstract Formula copy();
	/*
	 * Utilisé par toString
	 */

	protected String intToVar(int a) {
		return a > 0 ? "X" + a : "!X" + -a;
	}
	/*
	 * ne modifie pas l'instance 
	 */
	public AndFormula toCNFFormula(){
		Formula f = this.copy();
		f.reduire();
		f = f.toCNFFormulaREC();
		f.reduire();
		return (AndFormula)f;
	}

	
	public static IProblem cNFFormulaToSAT4J(AndFormula cnf, int nbVARs) throws ContradictionException{
		ISolver solveur = SolverFactory.newDefault();
		solveur.newVar(nbVARs);
		solveur.setExpectedNumberOfClauses(cnf.p.length);
		
		for(Formula f : cnf.p){
			if(f instanceof VarFormula){
				solveur.addClause(new VecInt(1).push(((VarFormula)f).var));
			}else{
				//alors f est un OR
				VecInt clause = new VecInt();
				for(Formula fp : ((OrFormula)f).p){
					clause.push(((VarFormula)fp).var);
				}
				solveur.addClause(clause);
			}
		}
		
		return solveur;
	}
}
