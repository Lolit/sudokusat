package fr.meyrat.sat.utils;

import java.util.ArrayList;
import java.util.Iterator;

public class OrFormula extends Formula {
	Formula[] p;

	public OrFormula(Formula... enfants) {
		p = enfants;
	}
	public OrFormula(int[] vals){
		p = new VarFormula[vals.length];
		for(int i=0; i<vals.length; i++){
			p[i] = new VarFormula(vals[i]);
		}
	}
	public OrFormula(ArrayList<Formula> vals){
		p = new Formula[vals.size()];
		Iterator<Formula> it = vals.iterator();
		int i=0;
		while(it.hasNext()){
			p[i] = it.next();
			i++;
		}
	}


	@Override
	public String toString() {
		String res = "";
		for (Formula q : p) {
			if (!(q instanceof VarFormula)) {
				res += "(" + q.toString() + ") OR ";
			} else {
				res += q.toString() + " OR ";
			}
		}
		return res.substring(0, res.length() - 3);
	}

	@Override
	public void reduire() {
		ArrayList<Formula> pc = new ArrayList<>();
		for (int i = 0; i < p.length; i++) {
			p[i].reduire();
			if (p[i] instanceof OrFormula) {
				for (Formula f : ((OrFormula) p[i]).p) {
					pc.add(f);
				}
			}else{
				pc.add(p[i]);
			}
		}
		Formula[] pcp = new Formula[pc.size()];
		p = pc.toArray(pcp);
	}

	@Override
	public Formula copy() {
		Formula[] pc = new Formula[p.length];
		for (int i = 0; i < p.length; i++) {
			pc[i] = p[i].copy();
		}
		return new OrFormula(pc);
	}


}
