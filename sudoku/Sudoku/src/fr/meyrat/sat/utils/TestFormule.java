package fr.meyrat.sat.utils;

import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.TimeoutException;

public class TestFormule {
	
	public static void main(String[] args){
		Formula f1 = new AndFormula(new Formula[]{new VarFormula(1), new VarFormula(-3), new VarFormula(-2)});
		Formula f2 = new OrFormula(new Formula[]{f1, new VarFormula(-1)});
	
		Formula formule = new AndFormula(new Formula[]{f1.copy(),f2, new VarFormula(3)});
		
		System.out.println(formule);
		
		formule = formule.toCNFFormula();
		
		System.out.println(formule);
		
		
		try {
			IProblem prob = Formula.cNFFormulaToSAT4J((AndFormula)formule, 3);
			if(prob.isSatisfiable()){
				System.out.println("Problème résoluble !");
				int[] solution = prob.model();
				for(int i =0; i< solution.length; i++){
					System.out.println("X"+(i+1)+"= "+(solution[i]>0));
				}
			}else{
				System.out.println("Problème non résoluble !");
			}
		} catch (TimeoutException e) {
			e.printStackTrace();
		} catch (ContradictionException e) {
			System.out.println("Trivialement impossible");
		}
	}

}
