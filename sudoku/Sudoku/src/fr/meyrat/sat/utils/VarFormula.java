package fr.meyrat.sat.utils;

public class VarFormula extends Formula {
    short var;
    public VarFormula(short a){
        var = a;
    }
    public VarFormula(int i) {
		var = (short)i;
	}
	@Override
    public String toString(){    
        return intToVar(var);
    }
	@Override
	public void reduire() {
		//Rien à faire !
	}
	@Override
	public Formula copy() {
		return new VarFormula(var);
	}

}
